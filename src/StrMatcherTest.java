import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class StrMatcherTest {
	@Test
	void anyCharacter() {
		assertTrue(StrMatcher.anyCharacter("a"));
		assertTrue(StrMatcher.anyCharacter("H"));
		assertTrue(StrMatcher.anyCharacter("6"));
		assertTrue(StrMatcher.anyCharacter("&"));
		assertTrue(StrMatcher.anyCharacter("$"));
		assertTrue(StrMatcher.anyCharacter("_"));
		assertTrue(StrMatcher.anyCharacter(" "));
		
		assertFalse(StrMatcher.anyCharacter(null));
		assertFalse(StrMatcher.anyCharacter(""));
		assertFalse(StrMatcher.anyCharacter("ahD"));
	}

	@Test
	void onceOrNotAtAll() {
		assertTrue(StrMatcher.onceOrNotAtAll("a"));
		assertTrue(StrMatcher.onceOrNotAtAll("H"));
		assertTrue(StrMatcher.onceOrNotAtAll("6"));
		assertTrue(StrMatcher.onceOrNotAtAll("&"));
		assertTrue(StrMatcher.onceOrNotAtAll("$"));
		assertTrue(StrMatcher.onceOrNotAtAll("_"));
		assertTrue(StrMatcher.onceOrNotAtAll(" "));
		
		assertTrue(StrMatcher.onceOrNotAtAll(""));
		
		assertFalse(StrMatcher.onceOrNotAtAll(null));
		assertFalse(StrMatcher.onceOrNotAtAll("ahD"));
	}
	
	@Test
	void zeroOrMoreTimes() {
		assertTrue(StrMatcher.zeroOrMoreTimes("a"));
		assertTrue(StrMatcher.zeroOrMoreTimes("ahD"));
		assertTrue(StrMatcher.zeroOrMoreTimes("H"));
		assertTrue(StrMatcher.zeroOrMoreTimes("6"));
		assertTrue(StrMatcher.zeroOrMoreTimes("&"));
		assertTrue(StrMatcher.zeroOrMoreTimes("$"));
		assertTrue(StrMatcher.zeroOrMoreTimes("_"));
		assertTrue(StrMatcher.zeroOrMoreTimes(" "));
		
		assertTrue(StrMatcher.zeroOrMoreTimes(""));
		
		assertFalse(StrMatcher.zeroOrMoreTimes(null));
	}
	
	@Test
	void oneOrMoreTimes() {
		assertTrue(StrMatcher.oneOrMoreTimes("a"));
		assertTrue(StrMatcher.oneOrMoreTimes("ahD"));
		assertTrue(StrMatcher.oneOrMoreTimes("H"));
		assertTrue(StrMatcher.oneOrMoreTimes("6"));
		assertTrue(StrMatcher.oneOrMoreTimes("&"));
		assertTrue(StrMatcher.oneOrMoreTimes("$"));
		assertTrue(StrMatcher.oneOrMoreTimes("_"));
		assertTrue(StrMatcher.oneOrMoreTimes(" "));
		
		assertFalse(StrMatcher.oneOrMoreTimes(""));
		
		assertFalse(StrMatcher.oneOrMoreTimes(null));
	}
	
	@Test
	void simpleClass() {
		assertTrue(StrMatcher.simpleClass("a"));
		assertTrue(StrMatcher.simpleClass("b"));
		assertTrue(StrMatcher.simpleClass("c"));
		assertFalse(StrMatcher.simpleClass("d"));
		assertFalse(StrMatcher.simpleClass("abc"));
	}
	
	
	@Test
	void classWithRange() {
		assertTrue(StrMatcher.classWithRange("a"));
		assertFalse(StrMatcher.classWithRange("ahD"));
		assertTrue(StrMatcher.classWithRange("H"));
		assertTrue(StrMatcher.classWithRange("6"));
		assertFalse(StrMatcher.classWithRange("&"));
		assertFalse(StrMatcher.classWithRange("$"));
		assertTrue(StrMatcher.classWithRange("_"));
		assertFalse(StrMatcher.classWithRange(" "));
		
		assertFalse(StrMatcher.classWithRange(""));
		
		assertFalse(StrMatcher.classWithRange(null));
	}
	
	@Test
	void classWithDigitRange() {
		assertFalse(StrMatcher.classWithDigitRange("a"));
		assertFalse(StrMatcher.classWithDigitRange("ahD"));
		assertFalse(StrMatcher.classWithDigitRange("H"));
		assertTrue(StrMatcher.classWithDigitRange("6"));
		assertFalse(StrMatcher.classWithDigitRange("5"));
		assertFalse(StrMatcher.classWithDigitRange("&"));
		assertFalse(StrMatcher.classWithDigitRange("$"));
		assertFalse(StrMatcher.classWithDigitRange("_"));
		assertFalse(StrMatcher.classWithDigitRange(" "));
		
		assertFalse(StrMatcher.classWithDigitRange(""));
		
		assertFalse(StrMatcher.classWithDigitRange(null));
	}
	
	@Test
	void test1() {
		assertTrue(StrMatcher.test1("abcdefg"));
		assertTrue(StrMatcher.test1("abcde"));
		assertTrue(StrMatcher.test1("abc"));
	}
	
	@Test
	void test2() {
		assertTrue(StrMatcher.test2("abc123xyz"));
		assertTrue(StrMatcher.test2("define \"123\""));
		assertTrue(StrMatcher.test2("var g = 123;"));
	}

	@Test
	void test3() {
		assertTrue(StrMatcher.test3("cat."));
		assertTrue(StrMatcher.test3(" 	896."));
		assertTrue(StrMatcher.test3(" 	?=+."));
		assertFalse(StrMatcher.test3("abc1"));
	}
	
	@Test
	void test4() {
		assertTrue(StrMatcher.test4("can"));
		assertTrue(StrMatcher.test4("man"));
		assertTrue(StrMatcher.test4("fan"));
		assertFalse(StrMatcher.test4("dan"));
		assertFalse(StrMatcher.test4("ran"));
		assertFalse(StrMatcher.test4("pan"));
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}