
public class Password {
	public static boolean passwordCheck(String psw) {
		return psw == null ? false : 
			psw.matches("(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[a-zA-Z0-9\\*\\$\\@\\%\\!\\_])[a-zA-Z0-9\\*\\$\\@\\%\\!\\_]{8,}");
	}
}
