public class StrMatcher {
	public static boolean anyCharacter(String text) {
		return text == null ? false : text.matches(".");
	}

	public static boolean onceOrNotAtAll(String text) {
		return text == null ? false : text.matches(".?");
	}

	public static boolean zeroOrMoreTimes(String text) {
		return text == null ? false : text.matches(".*");
	}

	public static boolean oneOrMoreTimes(String text) {
		return text == null ? false : text.matches(".+");
	}

	public static boolean test1(String text) {
		return text == null ? false : text.matches("abc.*");
	}
	
	public static boolean simpleClass(String text) {
		return text == null ? false : text.matches("[abc]");
	}
	
	public static boolean classWithRange(String text) {
		return text == null ? false : text.matches("\\w");
//		return text == null ? false : text.matches("[a-zA-Z0-9_]");
	}
	
	public static boolean classWithDigitRange(String text) {
		return text == null ? false : text.matches("[0-9&&[^5]]");
//		return text == null ? false : text.matches("\\d");
	}
	
	public static boolean test2(String text) {
//		return text == null ? false : text.matches(".*123.*");
//		return text == null ? false : text.matches("\\w+123\\w+");
//		return text == null ? false : text.matches("[^0-9]*123[^0-9]*");
		return text == null ? false : text.matches("(?=.*123).*");
	}
	
	public static boolean test3(String text) {
		return text == null ? false : text.matches("^.*\\.$");
	}
	
	public static boolean test4(String text) {
//		return text == null ? false : text.matches("[cmf]an");
		return text == null ? false : text.matches("[a-z&&[^drp]]an");
	}
}