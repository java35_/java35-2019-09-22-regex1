import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class PasswordTest {
	@Test
	public void rightPasswordTest() {
		assertTrue(Password.passwordCheck("ABc12_de"));
		assertTrue(Password.passwordCheck("123456A$a"));
		assertTrue(Password.passwordCheck("*****12cA"));
		assertFalse(Password.passwordCheck("12cA"));
	}
		
	@Test
	public void wrongLengthTest() {
		assertFalse(Password.passwordCheck("Ab1#"));
		assertFalse(Password.passwordCheck(""));
	}
	
	@Test
	public void wrongUpperCaseTest() {
		assertFalse(Password.passwordCheck("asda1222_q"));
	}
	
	@Test
	public void wrongLowerCaseTest() {
		assertFalse(Password.passwordCheck("ASSSD1222_M"));
	}
	
	@Test
	public void wrongDigit() {
		assertFalse(Password.passwordCheck("asdaASDF_q"));
	}
	
	@Test
	public void wrongSpecSymb() {
		assertFalse(Password.passwordCheck("asda1222q"));
	}
}
